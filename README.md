
# Quadratic Sieve

  

## Description

  

This code is meant to factor large numbers that are products of two distinct primes.

  

## Dependencies

Our code utilizes the numpy library, which you can install using the following command:

```
pip3 install numpy
```

## Running the Code

  

In order to factor a number n, you can simply run the following command:

```
python3 factor.py n

```

The program takes the following additional optional arguments in order to tune algorithm:

* b_tuning: a coefficient factor to adjust the value of B for evaluating B-smoothness (defaults to 1)

* A: the smoothness bound, ie. the number of numbers that are sieved (defaults to B^3)

* trial_division_register_threshold_factor: a factor to tune the number of registers to trial divide and check for B-smoothness after sieving; the threshold is equal to the product of B with this factor; the factor defaults to 10 (so the threshold defaults to 10*B)

  

So if you want to double the value of B, set A equal to 100000, and increase the threshold by a factor of 100, you can run the following command:

  

```
python3 factor.py n 2 100000 100

```

## Examples

Here are some sample cases that you can try

```
python3 factor.py 21 3 100 10
```

```
python3 factor.py 7067947793 1 10000 10
```

```
python3 factor.py 736055622283 1 35000 10
```

```
python3 factor.py 547983591439397 1 300000 10
```

```
python3 factor.py 16921456439215439701 1 3000000 10
```

```
python3 factor.py 92905709270744788219 1 150000 10
```

  

## Authors

  

Developed by Andrew Qin, Ina Ding, Sofia Hletko, Sravan Parimi

  

## Acknowledgment

Credit goes to Carl Pomerance for his "Smooth numbers and the quadratic sieve" paper for providing the logic and proto-algorithm for the quadratic sieve as well as to Cetin K Koc and Sarath N Arachchige for their "A Fast Gaussian Algorithm for Gaussian Elimination over GF(2) and Its Implementation on the GAPP*" paper for providing an overview of effective gaussian elimination over GF(2).