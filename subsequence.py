from typing import List, Tuple

def gaussianElimination(matrix):
    """
    Performs gaussian elimination on input matrix

    Parameters:
        matrix (ndarray): a 2 dimensional array with exponent vectors (mod 2) of b-smooth numbers

    Return:
        set: set of indices of the rows that do not contain pivots
        dict: dictionary that maps a given column index to the row index that contains the pivot for that column
    """
    numrows = matrix.shape[0]
    numcols = matrix.shape[1]
    nonPivotRows = set([_ for _ in range(numrows)])     # tracks which rows do not contain pivots (ie. form linearly dependent sets with other rows)
    pivotColToRow = {}              # tracks which row contains the pivot for a given col

    def clearRow(pivotcol, pivotrow):
        """
        Helper method: clears the pivot row using column addition operations

        Parameters:
            pivotcol: index of the column of the pivot under consideration
            pivotrow: index of the row of the pivot under consideration

        Returns:
            None: modifies outer matrix in place
        """
        # finds all of the row indices of elements that are 1 in the column under consideration
        oneIndices = [pivotrow]
        for row in range(pivotrow+1, numrows):      # starts after pivotrow because pivotrow must be the first occurence of the value 1
            if matrix[row][pivotcol] == 1:
                oneIndices.append(row)

        # add this column to each of the other columns that have 1 in the pivot row
        # for each such column, consider each element in the row indices that correspond to a 1 in the pivot column, and add 1 to those elements
        # no need to add 0
        for col in range(numcols):
            if col == pivotcol or matrix[pivotrow][col] != 1:
                continue
            for row in oneIndices:
                # adding 1 in mod 2 is equivalent to taking the logical not and casting to back int
                matrix[row][col] = int(not bool(matrix[row][col]))

    # iterate through each column, when the first 1 is found, clear that row
    for c in range(numcols):
        for r in range(numrows):
            if matrix[r][c] == 1:
                clearRow(c, r)
                nonPivotRows.discard(r)
                pivotColToRow[c] = r
                break
    
    return pivotColToRow, nonPivotRows

def findLinearlyDependentRows(matrix):
    """
    Generator for finding subsets of linearly dependent rows in a given mod 2 matrix
    
    Inspired by the procedure outlined by Cetin K. Koc and Sarath N. Arachchige in 
    "A Fast Algorithm for Gaussian Elimination over GF(2) and Its Implementation on the GAPP"
    https://www.cs.umd.edu/~gasarch/TOPICS/factoring/fastgauss.pdf


    Parameters:
        matrix (ndarray): a 2 dimensional array with exponent vectors (mod 2) of b-smooth numbers

    Returns:
        List[List[int]]: a list of lists of row indices of the matrix that are linearly dependent
    """
    numcols = matrix.shape[1]       # m in Sofia's psuedocode
    pivotColToRow, nonPivotRows = gaussianElimination(matrix)
    
    # for each non-pivot row, yield a set of linearly dependent rows by appending the nonpivot row and 
    # the corresponding pivot row for each column index for which the nonpivot row contains a nonzero element
    for rowIndex in nonPivotRows:
        linearlyDependentRows = [rowIndex]
        for colIndex in range(numcols):
            if matrix[rowIndex][colIndex] == 1:
                linearlyDependentRows.append(pivotColToRow[colIndex])
        yield linearlyDependentRows