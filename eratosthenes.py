##eratosthenes
import numpy as np
import math
#import pandas as pd

def create_factor_base(b): #b is the bound we chose

    prime_indices=np.array([True]*b) # we start with an array (quick to access elements, fixed length) of true/false, indicating whether the number at that index is true or false
    prime_indices[0]=False #0 is not prime
    prime_indices[1]=False #1 is not prime
    for i in range(2, math.isqrt(len(prime_indices))+1): #we do +1 to round up on the square root, we look for the multiples of i
        if prime_indices[i]: #if it has not been marked as false yet
            for j in range(i*i, b,i): #all the multiples of i less than bound b 
                prime_indices[j]=False# we now mark the multiples of i with itself as false, because they are multiples of a prime

    #now we have a boolean array, the value of each index is true if prime, false if not prime
    prime_numbers=[] #empty list to store the prime numbers
    for j in range(len(prime_indices)):
        # print("this is something new")
        if prime_indices[j]==True:
            prime_numbers.append(j) #add the index where it is True to the list of primes
    
    # print(f"prime numbers: {prime_numbers}")
    return prime_numbers

