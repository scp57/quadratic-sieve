import logging
from datetime import datetime

def init():
    nlist = [1649, 1457, 5959]
    xValsList = [[41, 42, 43, 47, 57],[39, 41, 43],[78, 80]]
    global start
    start = datetime.now()

def logtime(message=None):
    if message:
        logging.info("\t" + str(datetime.now()-start) + "\t\t" + message)
    else:
        logging.info(datetime.now()-start)