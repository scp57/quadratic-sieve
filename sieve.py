from subsequence import findLinearlyDependentRows
from eratosthenes import create_factor_base
import util
import math
from math import prod, gcd, isqrt, floor, log, sqrt
from numpy import array, empty, argsort
import logging

class QuadraticSieve:

    def __init__(self, n, b_tuning, trial_division_threshold, A):
        util.logtime("starting sieve")
        self.n = n
        self.find_b_smooth(self.n, b_tuning, trial_division_threshold, A)
    
    def solveExponentVector(self, vector):
        sol = 1
        for exp, prime in zip(vector, self.primesInFactorBase):
            sol *= prime**exp
        return sol

    def verifyandfactor(self, candidateSubsequences):
        congruenceFound = False

        for matrixRowIndices in candidateSubsequences:
            xSubSequence = [self.xVals[index] for index in matrixRowIndices]            
            xValsproduct = prod(xSubSequence)

            # calculate total power for each prime and divide by 2 for sqrt
            expVector = [0 for _ in self.primesInFactorBase]
            for primeIndex in range(len(expVector)):
                for rowIndex in matrixRowIndices:
                    expVector[primeIndex] += self.expVectorMatrix[rowIndex][primeIndex]
                if expVector[primeIndex] % 2 != 0:
                    print(f"ERROR: subsequence product is not a square: power of prime {self.primesInFactorBase[primeIndex]} is {expVector[primeIndex]}")
                expVector[primeIndex] = expVector[primeIndex] // 2
            subsequenceroot = self.solveExponentVector(expVector)

            a = xValsproduct % self.n
            b = subsequenceroot%self.n
            negb = (-subsequenceroot)%self.n
            
            logging.info(f"sqrts (a and b): {a}, {b}, {negb}")
            
            if a != b and a != negb:
                logging.debug(f"abs(a-b): {abs(a-b)}")

                p = gcd(int(a+b), self.n)
                q = int(self.n / p)

                if p != 1 and q != 1:
                    congruenceFound = True
                    break

        if not congruenceFound:
            logging.info("congruence not found, trying more b smooth")
            oldNumberOfSmoothVals = len(self.smoothVals)
            
            self.more_smooth()
            
            assert len(self.smoothVals) > oldNumberOfSmoothVals, "no new smooth values found"
            return self.verifyandfactor(findLinearlyDependentRows(self.mod2NumPyExpVecMatrix))
            
        return p, q

    def findFactors(self):
        p, q = self.verifyandfactor(findLinearlyDependentRows(self.mod2NumPyExpVecMatrix))
        assert p*q == self.n, f"factors found do not multiply to n. factor 1: {p}, factor 2: {q}, n: {self.n}"
        return p, q
    
    def find_b_smooth(self, n, btuning, registerThreshold=10, A=None):
        util.logtime("starting bsmooth")
        self.globalRegisterThreshold = registerThreshold

        self.B = int(btuning*floor(math.exp((1/2)*(sqrt(log(n)*log(log(n)))))))
        logging.info(f"\tvalue of B: {self.B}")
        factor_base = create_factor_base(self.B)    #create factor base

        if A is None:
             A = self.B**3
        self.s = isqrt(n) +1

        #list of primes p later to be removed from factor base
        toRemove = set()

        #1 x A array to store f(x)
        self.fx_array = empty(A)

        #1 x A array to store registers
        self.logfx_array = empty(A)

        util.logtime("initializing fx_array and logfx_array")

        startIndex = 0 if self.fx_array[0] % 2 == 0 else 1

        for x in range(len(self.logfx_array)):
                fx = ((x+self.s)**2)-n
                self.fx_array[x] = fx
                self.logfx_array[x] = math.log(fx,10)

        util.logtime("initialized numpy arrays for calculating bsmooth")

        # checks for x values where f(x) is even
        evenFx = 0
        log2 = math.log(2,10)
        for i in range(A):
                if self.fx_array[i]%2 ==0:      # if f(x) is even, subract log(2) from its register
                    evenFx = 1
                    self.logfx_array[i] -= log2
        
        # if there's no f(x) that's even, take out 2 from factor base
        if evenFx ==0:
                toRemove.add(2)

        util.logtime("finished checking even values")
        
        # for every p in factor base (besides 2, which was tested above), test 0<=x< p-1
        for p in factor_base:
                if p == 2:
                    continue
                xone = -1
                xtwo = -1
                for x in range(p-1):
                    if self.fx_array[x]%p ==0:    #fx_array holds fx values
                            #if found one x that satisfies f(x)=0 (mod p), save that x value
                            if xone == -1:
                                xone = x
                            else:
                                #if found two x that satisfy, suctract log(p) from registers
                                #  and subtract log(p) from registers of x value that are multiples of p from xone/xtwo
                                xtwo = x
                                logp = math.log(p,10)
                                self.logfx_array[xone] -= logp
                                for xs in range(xone,A,p):
                                        if xs != xone:
                                            self.logfx_array[xs] -= logp

                                self.logfx_array[xtwo] -= logp
                                for xs in range(xtwo,A,p):
                                        if xs != xtwo:
                                            self.logfx_array[xs] -= logp
                                
                                exp = 2
                                while (p**exp)<(self.B):
                                        for i in range((p**exp)-1):
                                            if self.fx_array[i]%(p**exp) == 0:
                                                    self.logfx_array[i] -= logp
                                                    for xs in range(i,A,p**exp):
                                                        self.logfx_array[i] -= logp
                                        exp+=1
                                break
                #if didn't find any x that satisfy, keep p to remove from factor base later
                if xone == -1:
                    toRemove.add(p)

        util.logtime("finished checking all primes")

        self.primesInFactorBase = []
        #find primes that are to be kept (stored in remainingP)
        for p in factor_base:
                if p not in toRemove:
                    self.primesInFactorBase.append(p)
        
        logging.info(f"\tsmooth primes: {self.primesInFactorBase}")
        
        util.logtime("sorting array")
        
        #obtain the indices that would sort our register values, in ascending order
        self.sorted_indices = argsort(self.logfx_array)
        
        #exponent matrix to keep track of exponent vectors for remaining f(x) values
        self.expVectorMatrix = []
        self.mod_two_exponent_matrix = []
        self.xVals = []
        self.smoothVals = []
        
        util.logtime("finished sorting, starting trial division")

        logging.info(f"\tremaining primes {self.primesInFactorBase}")

        # trial dividing f(x) with the lowest registers with primes in remainingP to test for smoothness
        for i in range(registerThreshold*self.B):
                x = self.sorted_indices[i]

                # initialize exponent vector for f(x)
                fx = self.fx_array[x]
                exponent_vector = [0 for _ in range(len(self.primesInFactorBase))]
                mod_2_exponent_vector = [0 for _ in range(len(self.primesInFactorBase))]
                
                # trial divide for each remaining prime in the factor base
                for j in range(len(self.primesInFactorBase)):
                    while fx%self.primesInFactorBase[j]==0:
                            fx = fx // self.primesInFactorBase[j]
                            exponent_vector[j]+=1
                
                    # exponent vector mod 2 for finding linear dependency
                    mod_2_exponent_vector[j] = exponent_vector[j]%2

                #if b-smooth, add corresponding x+s value to bsmooth numbers
                if fx == 1:
                    self.xVals.append(int(x+self.s))
                    self.smoothVals.append(self.fx_array[x])
                    self.expVectorMatrix.append(exponent_vector)
                    self.mod_two_exponent_matrix.append(mod_2_exponent_vector)
                
                # found enough b-smooth numbers
                if len(self.xVals)==len(self.primesInFactorBase)+1:
                    break
        
        self.stopIndex = i+1

        util.logtime(f"finished complete trial division, checked {self.stopIndex} registers")
        assert len(self.xVals) >= len(self.primesInFactorBase) + 1, "not enough bsmooth values found, try a higher B"

        logging.info(f"\tB-smooth x+s: {self.xVals}")
        logging.info(f"\texponent matrix: {self.mod_two_exponent_matrix}")

        util.logtime("verifying b_smooth")
        self.verify_b_smooth(self.smoothVals,  self.expVectorMatrix, self.primesInFactorBase)
        self.mod2NumPyExpVecMatrix = array(self.mod_two_exponent_matrix)

    def verify_b_smooth(self, numbers, exponent_vectors, primes):
        for num, vector in zip(numbers, exponent_vectors):
                if num != math.prod([prime**exp for prime, exp in zip(primes, vector)]):
                    print(f"num {num} is not b smooth!")
        logging.info("\tall numbers are smooth")

    def more_smooth(self):
        util.logtime("starting search for more bsmooth numbers")
        oldStopIndex = self.stopIndex
        oldExponentMatrixLen = len(self.expVectorMatrix)

        #if called from GE step where more exponent vectors are required, 
        for i in range(self.stopIndex, self.globalRegisterThreshold*self.B):
                x = self.sorted_indices[i]

                # initialize exponent vector for f(x)
                fx = self.fx_array[x]
                exponent_vector = [0 for _ in range(len(self.primesInFactorBase))]
                mod_2_exponent_vector = [0 for _ in range(len(self.primesInFactorBase))]
                
                # trial divide for each remaining prime in the factor base
                for j in range(len(self.primesInFactorBase)):
                    while fx%self.primesInFactorBase[j]==0:
                            fx = fx // self.primesInFactorBase[j]
                            exponent_vector[j]+=1
                
                    # exponent vector mod 2 for finding linear dependency
                    mod_2_exponent_vector[j] = exponent_vector[j]%2

                #if b-smooth, add corresponding x+s value to bsmooth numbers
                if fx == 1:
                    self.xVals.append(int(x+self.s))
                    self.smoothVals.append(self.fx_array[x])
                    self.expVectorMatrix.append(exponent_vector)
                    self.mod_two_exponent_matrix.append(mod_2_exponent_vector)
                
                # found enough b-smooth numbers
                if len(self.xVals)==len(self.primesInFactorBase)+11:
                    break
        
        self.stopIndex = i+1
        util.logtime(f"finished complete trial division, checked {self.stopIndex-oldStopIndex} registers")
        
        assert len(self.expVectorMatrix) > oldExponentMatrixLen, "no more b smooth values found: try adjusting either the B value or the register threshold"
        
        util.logtime("verifying b_smooth")
        self.verify_b_smooth(self.smoothVals,  self.expVectorMatrix, self.primesInFactorBase)
        self.mod2NumPyExpVecMatrix = array(self.mod_two_exponent_matrix)