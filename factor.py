from sieve import QuadraticSieve
import sys
import util
from datetime import datetime

if len(sys.argv) < 2 or len(sys.argv) > 5:
    print("Syntax:\n"
          "\tpython3 factor.py <number> [<b_tuning_factor>] [<trial_division_register_threshold_factor]\n"
          "Arguments:\n"
          "\t<number>: integer to be factored\n"
          "\t<b_tuning_factor>: factor to multiply B by for tuning the value; defaults to 1\n"
          "\t[<A>]: number of values to sieve through, defaults to B^3"
          "\t<trial_division_register_threshold_factor>]: # factor to tune the number of registers to trial divide after sieving; the factor is multiplied by B to get the final threshold; factor defaults to 10 (so the threshold defaults to 10*B)")
    sys.exit()

# logging.basicConfig(level=logging.INFO)
util.init()

n = int(sys.argv[1])
b_tuning = float(sys.argv[2]) if len(sys.argv) > 2 else 1.0
A = int(sys.argv[3]) if len(sys.argv) > 3 else None
trial_division_register_threshold = int(sys.argv[4]) if len(sys.argv) > 4 else 10

s = QuadraticSieve(n, b_tuning, trial_division_register_threshold, A)
factor1, factor2 = s.findFactors()
print(f"factors: {factor1}, {factor2}")
print("Elapsed Time:", str(datetime.now()-util.start))